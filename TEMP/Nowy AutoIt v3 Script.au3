#include <GuiConstants.au3>

$GuiHandle = GUICreate("Progress Bar Test", 300, 130)
$ProgBar_1 = GUICtrlCreateProgress(10, 10, 280, 30, $PBS_Smooth)
$UpButton = GUICtrlCreateButton("UP", 15, 50, 76, 40)
$DownButton = GUICtrlCreateButton("Down", 112, 50, 76, 40)
$StopButton = GUICtrlCreateButton("STOP", 209, 50, 76, 40)
$Label_1 = GUICtrlCreateLabel("Red = 0, Green = 0, Blue = 0, Color = 0xFF0000", 10, 100, 280, 20, $SS_Center)
GUISetState()

$Progress = 0; Will be 0 thru 1000
$Increment = 0
While(1)
    $GuiMsg = GUIGetMsg()
    Select
        Case $GuiMsg = $GUI_EVENT_CLOSE
            Exit
        Case $GuiMsg = $UpButton
            $Increment = +1
        Case $GuiMsg = $DownButton
            $Increment = -1
        Case $GuiMsg = $StopButton
            $Increment = 0
        EndSelect
    _SetProgColor()
WEnd

Func _SetProgColor()
    $Progress = $Progress + $Increment
    If $Progress < 0 Then $Progress = 0
    If $Progress > 1000 Then $Progress = 1000
    GUICtrlSetData($ProgBar_1, Int($Progress / 10))

    $Redness = Int(255 - ($Progress / 1000 * 512))
    If $Redness < 0 Then $Redness = 0

    $Greeness = Int(($Progress / 1000 * 512) - 257)
    If $Greeness < 0 Then $Greeness = 0

    $Blueness = Int(255 - ($Redness + $Greeness))

    $ProgColor = ($Redness * 256 * 256) + ($Greeness * 256) + $Blueness
    GUICtrlSetData($Label_1, "Red = " & $Redness & ", Green = " & $Greeness & ", Blue = " & $Blueness & ", Color = 0x" & Hex($ProgColor, 6))
    GUICtrlSetColor($ProgBar_1, $ProgColor)
EndFunc