#RequireAdmin
#include <NomadMemory.au3>




main()


Func main()
   $pid = "6380"
   Local $maxHP_offsets[] = [0x728, 0x1B4, 0x7B4, 0x584, 0x670]
   Local $currentHP_offsets[] = [0x728, 0x1B4, 0x7B4, 0x584, 0x678]
   $current_hp = scan($pid, $currentHP_offsets)
   $max_hp = scan($pid, $maxHP_offsets)

   MsgBox(0, "maxHP: " & $current_hp, "Current hp: " + $max_hp)
EndFunc

Func scan($pid, $offsets)
   ; Core.dll = 0x10100000
   ; NWindow.DLL = 0x0AC60000
   ; Core.dll static offset: 0x0013C698
   ; NWindow static offset: 0x00381140
   $core = 0x10100000
   $static_offset = 0x0013C698

   $memory = _MemoryOpen(ProcessExists($pid))
	  $value = $core + $static_offset
	  For $offset in $offsets
		 $value = _MemoryRead($value, $memory, 'dword') + $offset
	  Next
	  $value = _MemoryRead($value, $memory, 'dword')
   _MemoryClose($memory)

   return $value
EndFunc

