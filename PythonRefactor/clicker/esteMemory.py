# Python 2.7.13

import psutil
import ctypes
from helper import pointers

class OpenMemory:
    OpenProcess = ctypes.windll.kernel32.OpenProcess
    ReadProcessMemory = ctypes.windll.kernel32.ReadProcessMemory
    WriteProcessMemory = ctypes.windll.kernel32.WriteProcessMemory
    CloseHandle = ctypes.windll.kernel32.CloseHandle
    PROCESS_ALL_ACCESS = 0x1F0FFF

    def __init__(self, nameOrPid):
        if isinstance(nameOrPid, int):
            self.pid = nameOrPid
        else:
            self.pid = self.getPid(nameOrPid)
        if self.pid:
            self.processHandler = self.OpenProcess(self.PROCESS_ALL_ACCESS, False, self.pid)

    def readMemory(self, memoryAddress=0x00000000):
        content = ctypes.c_uint()
        buffer = ctypes.byref(content)
        bufferSize = ctypes.sizeof(content)
        bytesRead = ctypes.c_ulong(0)

        if self.ReadProcessMemory(self.processHandler, memoryAddress, buffer, bufferSize, bytesRead):
            return content.value

    def writeMemory(self, memoryAddress, value):
        content = ctypes.c_uint(value)
        buffer = ctypes.byref(content)
        bufferSize = ctypes.sizeof(content)
        bytesRead = ctypes.c_ulong(0)

        self.WriteProcessMemory(self.processHandler, memoryAddress, buffer, bufferSize, bytesRead)

    def readMemoryWithOffsets(self, nwindowdll=None, static_offset=None, offsets=None):
        local_nwindowdll = nwindowdll
        local_static_offset = static_offset
        local_offsets = offsets
        if not nwindowdll:
            local_nwindowdll = pointers.coredll
        if not static_offset:
            local_static_offset = pointers.static_coredll
        if not offsets:
            local_offsets = pointers.basic_offset
        try:
            value = self.readMemory(local_nwindowdll + local_static_offset)
            for offset in local_offsets:
                value = self.readMemory(value + offset)
            return value
        except Exception:
            return 0

    def getPid(self, processName):
        for process in psutil.process_iter():
            if process.name() == processName:
                return process.pid

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.CloseHandle(self.processHandler)


def getNwindowdll(pid):
    '''
    :param pid:
    :return: return hex (value in int) for nwindowdll.
    Works only if we are logged in game with character.
    '''
    value = 9460301
    possibles = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'A', 'B', 'C', 'D', 'E', 'F')
    with OpenMemory(pid) as con:
        for el1 in possibles:
            for el2 in possibles:
                for el3 in possibles:
                    tmp = "0x0" + el1 + el2 + el3 + "0000"
                    convert_hex = int(tmp, 16)
                    if con.readMemory(convert_hex) == value:
                        val = con.readMemoryWithOffsets(nwindowdll=convert_hex)
                        if val > 0 and convert_hex > 0:
                            return convert_hex
    return None


if __name__ == '__main__':
    nwindow_in_hex = getNwindowdll(4500)
    print nwindow_in_hex
