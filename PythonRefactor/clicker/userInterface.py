# Python 2.7.13

from tkinter import *
from tkinter.ttk import *
from helper import getProcessStatus
from externals.AutoIt import checkWindow
from esteMemory import OpenMemory, getNwindowdll
from helper import pointers
import tkinter.messagebox
import thread
import random
import time
from externals import AutoIt
from externals.Cardinal import Cardinal
from externals.bountyHunter import BountyHunter
from collections import OrderedDict
import keyboard

comboPidValues = ('l2.exe', 'l2.bin')

def checkWin():
    try:
        selection = pidList.curselection()
        selectedPid = pidList.get(selection)
        checkWindow(selectedPid)

    except Exception as e:
        tkinter.messagebox.showinfo('Check Window', 'You did not pick a PID from ListBox. \n' + e.message)

def searchPids():
    name = comboPid.get()
    elements = getProcessStatus(name)
    pidList.delete(0, "end")
    for index, tuple in enumerate(elements):
        pid, _, _ = tuple
        pidList.insert(index, pid)
    for el in b.buttons:
        process, _, _ = el
        process['values'] = pidList.get(0, "end")

class buttonMap:
    def __init__(self, root):
        self.buttons = list()
        self.frame = Frame(root, width=500, height=500)
        self.frame.pack()
        for index in xrange(12):
            self.buttons.append(self.hotkey(index*25))

    def printMessage(self):
        print("HI")

    def hotkey(self, index):
        processCombo = Combobox(self.frame)
        processCombo.place(x=0, y=100 + index)

        processhotkeycombo = Combobox(self.frame)
        processhotkeycombo['values'] = ('1','2','3','4','5','6','7','8','9','0','q','w','e','r','t','y','u','i','o','p')
        processhotkeycombo.place(x=150,  y=100 + index)

        hotkeyCombo = Combobox(self.frame)
        hotkeyCombo['values'] = ('F1', 'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9', 'F10', 'F11', 'F12')
        hotkeyCombo.place(x=300, y=100 + index)

        return (processCombo, processhotkeycombo, hotkeyCombo)

class mainClass():
    def __init__(self, root):
        self.root = root
        self.running = False
        self.parametersPerPid = OrderedDict()
        self.nwindowdllPerPid = dict()
        self.characterNamePerPid = dict()
        self.pidPerCharacterName = dict()

        self.runButton = Button(self.root, text="start", command=self.changeState)
        self.runButton.pack()
        self.canvas = Canvas(root, width=400, height=500)
        self.canvas.pack()


    def changeState(self):
        if self.running:
            self.running = False
            self.runButton['text'] = "Start"
        else:
            self.running = True
            self.runButton['text'] = "Running..."

            thread.start_new_thread(self.getParameters, (None,))
            thread.start_new_thread(self.startBishop, (self.pidPerCharacterName["Enough"],))
            thread.start_new_thread(self.startBountyHunter, (self.pidPerCharacterName["DangMe"], self.pidPerCharacterName["Conflux"]))
            # thread.start_new_thread(self.mapButtons, (None,))

    def mapButtons(self, *args):
        while self.running:
            for map in b.buttons:
                process, key, hotkey = map[0].get(), map[1].get(), map[2].get()

                if process and keyboard.is_pressed(key):

                    AutoIt.send(process, '{'+hotkey+'}')

                    break
            time.sleep(0.1)

        print "Turned off the threads."


    def startBishop(self, bishopPid):
        bishop = Cardinal(bishopPid)
        while self.running:
            bishop.update(self.parametersPerPid, self.characterNamePerPid)
            time.sleep(1)
        print "Bishop Thread is stopped."


    def startBountyHunter(self, bountyHunterPid):
        bh = BountyHunter(bountyHunterPid)
        while self.running:
            bh.update(self.parametersPerPid, self.characterNamePerPid)
            time.sleep(1)
        print "BountyHunter Thread is stopped."


    def repaint(self):
        self.canvas.delete(ALL)
        light_red = "#ef9999"
        red = "#d80000"
        light_blue = "#7fcce5"
        blue = "#0099cc"
        counter = 0
        for pid, values in self.parametersPerPid.iteritems():
            maxHP, currentHP, maxMP, currentMP, _ = values
            print pid, values
            percentHP = currentHP * 1.0 / (maxHP+1)
            percentMP = currentMP * 1.0 / (maxMP+1)

            self.canvas.create_rectangle(5, 5+counter, 400, 17+counter, outline=light_red, fill=light_red)
            self.canvas.create_rectangle(7, 7+counter, 398 * percentHP, 15+counter, outline=light_red, fill=red)

            self.canvas.create_rectangle(5, 20+counter, 400, 32+counter, outline=light_blue, fill=light_blue)
            self.canvas.create_rectangle(7, 22+counter, 398 * percentMP, 30+counter, outline=light_blue, fill=blue)

            counter += 40

    def getParameters(self, *args):
        while self.running:
            for index, pid in enumerate(pidList.get(0, "end")):
                if not (pid in self.characterNamePerPid):
                    # print "Did not find pid:" + str(pid) + " with mapped nwindowdll address!"
                    # print "Trying get nwindowdll memory address..."
                    # nwindowdll = getNwindowdll(pid)
                    # if nwindowdll:
                    #     print "successfully! Got an address with int representation: ", nwindowdll
                    #     self.nwindowdllPerPid[pid] = nwindowdll
                    if entryList[index]:
                        self.characterNamePerPid[pid] = entryList[index].get()
                        self.pidPerCharacterName[entryList[index].get()] = pid
                else:

                    with OpenMemory(pid) as con:
                        targetHP = con.readMemoryWithOffsets(offsets=pointers.targetHP)
                        maxHP = con.readMemoryWithOffsets(offsets=pointers.maxHP)
                        currentHP = con.readMemoryWithOffsets(offsets=pointers.currentHP)
                        maxMP = con.readMemoryWithOffsets(offsets=pointers.maxMP)
                        currentMP = con.readMemoryWithOffsets(offsets=pointers.currentMP)

                        self.parametersPerPid[pid] = (maxHP, currentHP, maxMP, currentMP, targetHP)
                    # print "reading memory for pid:"+str(pid)+"... "
            self.repaint()
            time.sleep(1)
        print "Closed thread: getParameters"

root = Tk()
comboPid = Combobox(root)
comboPid['values'] = comboPidValues
comboPid.current(0)
comboPid.pack()

pidList = Listbox(root)
pidList.pack()

entryList = list()
for i in range(6):
    entry = Entry(root)
    entry.pack()
    entryList.append(entry)



Button(root, text="scan", command=searchPids).pack()
Button(root, text="Check Window", command=checkWin).pack()

b = buttonMap(root)


mainClass(root)
root.mainloop()