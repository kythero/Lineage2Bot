from tkinter import *



def doNothing():
    print "Ok, done"


root = Tk()

menu = Menu(root)
root.config(menu=menu)


subMenu = Menu(menu)

menu.add_cascade(label="File", menu=subMenu)
subMenu.add_command(label="New Project...", command=doNothing)
subMenu.add_command(label="New...", command=doNothing)
subMenu.add_separator()
subMenu.add_command(label="Exit", command=doNothing)

editMenu = Menu(menu)
menu.add_cascade(label="Edit", menu=editMenu)
editMenu.add_command(label="Read", command=doNothing)




################### Toolbar

toolbar = Frame(root, bg="black")
insertButton = Button(toolbar, text="Insert object", command=doNothing)
insertButton.pack(side=LEFT, padx=2, pady=2)
printButton = Button(toolbar, text="Print", command=doNothing)
printButton.pack(side=TOP, fill=X)

toolbar.pack()


# Status BAR

status = Label(root, text="Preparing to do nothing.", bd=1, relief=SUNKEN, anchor=W)
status.pack(side=BOTTOM, fill=X)




root.mainloop()