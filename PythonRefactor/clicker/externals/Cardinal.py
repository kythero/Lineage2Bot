
from externals.AutoIt import AutoIt
import time
from statistics import median

class Cardinal(AutoIt):

    PRAYER_DELAY = 0.7
    SOE_DELAY = 25
    MASS_RESSURECTION = "{F1}"
    RESSURECTION = "{F2}"
    PRAYER = "{F3}"
    MAJOR_GROUP_HEAL = "{F4}"
    GREATER_BATTLE_HEAL = "{F5}"
    MAJOR_HEAL = "{F6}"
    BALANCE_LIFE = "{F7}"
    RESTORE_LIFE = "{F8}"
    CELESTIAL_SHIELD = "{F9}"
    MP_POTS = "{F10}"
    CLEANSE = "{F11}"
    SOE = "{F12}"

    def __init__(self, pid=None):
        super(Cardinal, self).__init__(pid)
        self.prayer_timer = time.time()
        self.escaping = False
        self.prayer()

    def update(self, data, characters):
        delta_prayer = time.time() - self.prayer_timer
        delta_follow = time.time() - self.follow_timer
        if delta_prayer > 100:
            self.prayer()
        if delta_follow > 12:
            self.follow_timer = time.time()
            self.follow()


        currentHPList = list()
        percentList = list()
        for pid, values in data.iteritems():
            maxHP, currentHP, _, _, _ = values
            percent = currentHP * 100.0 / (maxHP+1)
            currentHPList.append((pid, currentHP))
            percentList.append((pid, percent))

        percentList.sort(key=lambda tup: tup[1])

        hpElements = [tup[1] for tup in percentList]
        mediana = median(hpElements)
        average = self.average(*hpElements)
        if percentList:
            pid, percentHP = percentList[0]

            if percentHP == 0:
                self.ressurection(characters[pid])

            if average < 75 and mediana > 95:
                self.balanceLife()
            elif average < 70:
                self.majorGroupHeal()

            elif percentHP < 65:
                characterName = characters[pid]
                self.greaterBattleHeal(characterName)

            elif percentHP < 85:
                characterName = characters[pid]
                self.majorHeal(characterName)

            else:
                self.follower = "Conflux"
        self.clearChat()

    def massRessurection(self):
        self.send(self.MASS_RESSURECTION)

    def ressurection(self, characterName=""):
        self.target(characterName)
        self.send(self.RESSURECTION)

    def prayer(self):
        self.prayer_timer = time.time()
        self.send(self.PRAYER)
        time.sleep(self.PRAYER_DELAY)

    def majorGroupHeal(self):
        self.send(self.MAJOR_GROUP_HEAL)

    def greaterBattleHeal(self, characterName=""):
        self.target(characterName)
        self.send(self.GREATER_BATTLE_HEAL)

    def majorHeal(self, characterName=""):
        self.target(characterName)
        self.send(self.MAJOR_HEAL)

    def balanceLife(self):
        self.send(self.BALANCE_LIFE)

    def restoreLife(self, characterName=""):
        self.target(characterName)
        self.send(self.RESTORE_LIFE)

    def celestialShield(self, characterName=""):
        self.target(characterName)
        self.send(self.CELESTIAL_SHIELD)

    def mpPotions(self):
        self.send(self.MP_POTS)

    def cleanse(self, characterName=""):
        self.target(characterName)
        self.send(self.CLEANSE)

    def soe(self):
        self.send(self.SOE)
        self.escaping = True