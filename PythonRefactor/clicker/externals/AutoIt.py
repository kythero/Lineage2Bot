# Python 2.7.13

import os
import time

PATTERN_FOR_AUTOIT_WINLIST = "Lineage"

class AutoIt(object):

    TARGET_DELAY = 0.7
    ASSIST_DELAY = 0.5
    SHORT_DELAY = 0.3

    def __init__(self, pid=None):
        if pid == None:
            raise Exception('missing "pid" parameter.')
        self.pid = str(pid)
        self.follower = None
        self.follow_timer = time.time()
        self.follow()
        self.assister = None
        self.targeter = None

    def send(self, string=""):
        command = ' '.join((
                        'externals\controlSend.exe',
                        str(self.pid),
                        '"' + string + '"',
                        PATTERN_FOR_AUTOIT_WINLIST))
        os.system(command)

    def target(self, characterName=""):
        self.send("/target "+characterName)
        time.sleep(self.TARGET_DELAY)
        self.send("{ENTER}")

    def follow(self, characterName=""):
        charName = characterName
        if self.follower:
            charName = self.follower
        if charName:
            self.target(charName)
            self.target(charName)
            self.clearChat()

    def assist(self, characterName=""):
        self.target(characterName)
        self.send("/assist")
        time.sleep(self.SHORT_DELAY)
        self.send("{ENTER}")
        time.sleep(self.ASSIST_DELAY)

    def clearChat(self):
        self.send("{ENTER}")
        self.send("{ENTER}")

    def trade(self, characterName=""):
        self.target(characterName)
        self.send("/trade")
        time.sleep(self.SHORT_DELAY)
        self.send("{ENTER}")

    def average(self, *args):
        count = len(args)
        sum = 0
        for arg in args:
            sum += arg
        return sum/count



def checkWindow(pid=""):
    command = ' '.join((
                'externals\checkWindow.exe',
                str(pid),
                PATTERN_FOR_AUTOIT_WINLIST
                ))
    os.system(command)

def send(pid, string=""):
    command = ' '.join((
                    'externals\controlSend.exe',
                    str(pid),
                    '"' + string + '"',
                    PATTERN_FOR_AUTOIT_WINLIST))
    os.system(command)