
from externals.AutoIt import AutoIt

class BountyHunter(AutoIt):

    SOE_DELAY = 25
    SWEEP_DELAY = 1
    SPOIL_DELAY = 7
    NEXT_TARGET = "{F4}"
    ATTACK = "{F5}"
    SPOIL = "{F6}"
    SWEEP = "{F7}"
    SOE = "{F12}"

    def __init__(self, pid=None, mainAssisterPid=None):
        super(BountyHunter, self).__init__(pid)
        self.spoilTargeter = None
        self.escaping = False
        self.maPid = str(mainAssisterPid)
        self.rememberTarget = None
        self.spoil_count = 0
        self.spoil_timer = time.time()

    def update(self, data, characters):
        delta_follow = time.time() - self.follow_timer
        if delta_follow > 12:
            self.follow_timer = time.time()
            self.follow()

        for pid, values in data.iteritems():
            if not (pid == self.maPid):
                continue
            maxHP, currentHP, _, _, targetHP = values
            if maxHP == targetHP:
                continue

            if not self.rememberTarget:
                self.rememberTarget = targetHP
            else:
                if targetHP > self.rememberTarget + 100:
                    self.rememberTarget = targetHP
                    self.spoil_count = 0

            delta_spoil = time.time() - self.spoil_timer
            if (delta_spoil > self.SPOIL_DELAY) or self.spoil_count == 0:
                if self.rememberTarget > 1000 and self.spoil_count < 2:
                    self.spoil_timer = time.time()
                    self.spoil_count += 1
                    self.spoil(characters[self.maPid])

            if self.rememberTarget <= 0:
                self.sweep()
                self.sweep()
                self.spoil_count = 0

        self.clearChat()


    def spoil(self, characterName=""):
        char = characterName
        if self.spoilTargeter:
            char = self.spoilTargeter
        self.assist(char)

    def sweep(self):
        self.send(self.SWEEP)
        time.sleep(self.SWEEP_DELAY)

    def soe(self):
        self.send(self.SOE)
        self.escaping = True





