# Python 2.7.13

import psutil


class pointers:
    coredll = 0x10100000
    static_coredll = 0x0013C698
    #nwindowdll = #0x0AFF0000 #
    static_offset_HP = 0x0013C698#0x0022CD70 #0x00394344
    static_offset_MP = 0x0013C698
    feline = 3740
    # basic_offset = (0x4C0, 0x4F4, 0x750, 0x4B4, 0x678)
    #
    # maxHP = (0x68C, 0x53C, 0x4A4, 0x5D4, 0x670)
    # currentHP = (0x68C, 0x53C, 0x4A4, 0x5D4, 0x678)
    #
    # maxMP = (0x4C0, 0x4F4, 0x750, 0x4B4, 0x670)
    # currentMP = (0x4C0, 0x4F4, 0x750, 0x4B4, 0x678)
    basic_offset = (0x4, 0x2E8, 0x200, 0x64, 0x678)
    # maxHP = (0x8, 0x8, 0x1F4, 0x388, 0x670)
    # currentHP = (0x8, 0x8, 0x1F4, 0x388, 0x678)

    targetHP = (0x728, 0x1B4, 0x410, 0x14C, 0x5DC)
    maxHP = (0x4, 0x2E8, 0x200, 0x64, 0x678)
    currentHP = (0x4, 0x2E8, 0x200, 0x64, 0x678)

    maxMP = (0x4, 0x2E8, 0x200, 0x44, 0x670)
    currentMP = (0x4, 0x2E8, 0x200, 0x44, 0x678)

def getProcessStatus(name):
    '''
    :param name: name of process, eg: "l2.exe"
    :return: list of tuples, which contain (pid, name, time_created)
    '''
    processList = list()
    for process in psutil.process_iter():
        if process.name() == name:
            processList.append((process.pid, process.name(), process.create_time()))
    return sorted(processList, key=lambda x: x[2])





if __name__ == '__main__':

    list = getProcessStatus("l2.exe")
    for l in list:
        print l