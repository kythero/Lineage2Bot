#include-once
#include <FileConstants.au3>

; Dict meethods:
; Dict.remove   Dict.removeAll
; Dict.add
; Dict.item (get value)
; Dict.keys (all keys)
; Dict.Items (all values)
; Dict.exists

Func dict()
   $dict = ObjCreate("Scripting.Dictionary")
   If @error Then
	  Return @error
   Else
	  Return $dict
   EndIf
EndFunc

Func dict_add($dict, $key, $value)
   If $dict.exists($key) Then
	  $dict.item($key) = $value
   Else
	  $dict.add($key, $value)
   EndIf
EndFunc

Func dict_remove_keys($dict, $keys)
   If VarGetType($keys) == "Array" Then
	  For $el in $keys
		 If $dict.exists($keys) Then $dict.remove($keys)
	  Next
   Else
	  If $dict.exists($keys) Then $dict.remove($keys)
   EndIf
EndFunc

Func dict_update($dict, $key, $value)
   If $dict.exists($key) Then
	  $dict.item($key) = $value
   Else
	  $dict.add($key, $value)
   EndIf
EndFunc

Func dict_remove_values($dict, $values)
   If VarGetType($values) == "Array" Then
	  For $value in $values
		 For $key in $dict.keys
			If $dict.item($key) == $value Then $dict.remove($key)
		 Next
	  Next
   Else
	  If $dict.item($key) == $values Then $dict.remove($key)
   EndIf
EndFunc

Func dict_array_2D($dict, $array, $swap = False, $start_row = 0)
   For $row = $start_row To UBound($array) - 1
	  If not $swap Then
		 dict_add($dict, $array[$row][0], $array[$row][1])
	  Else
		 dict_add($dict, $array[$row][1], $array[$row][0])
	  EndIf
   Next
EndFunc

Func dict_update_array($dict, $key, $index, $value)
   Local $tmp = $dict.item($key)
   If VarGetType($tmp) == "Array" and $index < UBound($tmp) Then
	  $tmp[$index] = $value
	  dict_update($dict, $key, $tmp)
   Else
	  return setError(3, 10, "Index is out of range")
   EndIf
EndFunc

Func dict_print($dict, $console = True)
   For $key in $dict.keys
	  If $console Then
		 ConsoleWrite("Key: Value -> " & $key & ": " & $dict.item($key) & @CRLF)
	  Else
		 MsgBox(0, "Dictionary.",  $key & " : " & $dict.item($key), 0.5)
	  EndIf
   Next
EndFunc