#include-once
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <ColorConstants.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <ProgressConstants.au3>
#include <FileConstants.au3>
#include <MsgBoxConstants.au3>





$WINDOW_TITLE = "ESTE"
$WINDOW_WIDTH = 550
$WINDOW_HEIGHT = 350
$WINDOW_POSX = 200
$WINDOW_POSY = 200

$OPTION_SCAN = "Scan"
$OPTION_PROCESS_TEXT = "L2.exe"
$OPTION_SHOW = "Check Window"
$OPTION_CLEAR = "Clear list"
$REGISTERED_BISHOP = 0
$RUN_KEY_MAPPING = 0
$CRAFTING = 0
$SPOIL = 0
$REGISTERED_SPOILER = 0


$form = GUICreate($WINDOW_TITLE, $WINDOW_WIDTH, $WINDOW_HEIGHT)
   GUISetBkColor('0x03477F')
    $process_name = GUICtrlCreateCombo($OPTION_PROCESS_TEXT, 20, 20, 77, 17)
	GUICtrlSetData(-1, 'L2.bin')
	modify_combos(-1)
    $process_list = GUICtrlCreateList("", 100, 20, 77, 43)
    $process_show = GUICtrlCreateButton($OPTION_SHOW, 180, 20, 77, 17)
   modify_buttons(-1)


    $process_scan = GUICtrlCreateButton($OPTION_SCAN, 20, 40, 77, 17)
   modify_buttons(-1)
    $process_clear = GUICtrlCreateButton($OPTION_CLEAR, 180, 40, 77, 17)
   modify_buttons(-1)

    For $i = 0 To 11 Step 1
        $process_pool[$i] = GuiCtrlCreateCombo("", 20, 60 + $i*20, 57, 17)
		modify_combos(-1)
        $hotkey_pool[$i] = GuiCtrlCreateInput("", 100, 60 + $i*20, 57, 17)
		modify_combos(-1)
        $skill_pool[$i] = GuiCtrlCreateCombo("", 180, 60 + $i*20, 57, 17)
		modify_combos(-1)
	 Next
	 For $i=0 To 5 Step 1
	    $status_pid[$i] = GUICtrlCreateCombo("", 480, 10 + 45 * $i, 50, 17)
		modify_combos(-1)
     Next

    $process_start = GUICtrlCreateButton("IDLE", 50, 310, 157, 17)
	modify_buttons(-1)
	$craft_start = GUICtrlCreateButton("craft", 210, 310, 50, 17)
	modify_buttons(-1)
	$craft_heal = GUICtrlCreateButton("mouse heal", 270, 310, 70, 17)
	modify_buttons(-1)
	GUICtrlSetBkColor(-1, $BLUE[0])
	$craft_craft = GUICtrlCreateButton("mouse craft", 270, 330, 70, 17)
	modify_buttons(-1)
	GUICtrlSetBkColor(-1, $BLUE[0])

   $spoil_start = GUICtrlCreateButton("start spoil", 380, 290, 70, 17)
   modify_buttons(-1)
   $input_follow = GUICtrlCreateInput("", 380, 310, 70, 17)
   modify_combos(-1)
   $bounty_hunter = GUICtrlCreateInput("", 380, 330, 70, 17)
   modify_combos(-1)

	$activate_bishop = GUICtrlCreateButton("Bishop", 20, 330, 140, 17)
	modify_buttons(-1)
	GUICtrlSetBkColor(-1, $BLUE[0])

	$activate_bishop_pid = GUICtrlCreateInput("", 180, 330, 50, 17)
	$pick_bishop_pid = GUICtrlCreateButton('pick', 230, 330, 30, 17)
	modify_buttons(-1)
	  GUICtrlSetBkColor(-1, $BLUE[0])

	 For $i = 0 To 11 Step 1
		GUICtrlSetData($skill_pool[$i], 'F1|F2|F3|F4|F5|F6|F7|F8|F9|F10|F11|F12')
     Next

     GUISetState(@SW_SHOW)
