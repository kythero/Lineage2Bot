;~ #RequireAdmin
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <ColorConstants.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <ProgressConstants.au3>
#include <FileConstants.au3>
#include <MsgBoxConstants.au3>
#include <Array.au3>
#include <math.au3>
#include <Misc.au3>
#include "libs/udf.au3"
#include "libs/dictionary.au3"

Local $dict_pid = dict()
Local $dict_states = dict()

Local $dict_spoil_keys = dict()
dict_add($dict_spoil_keys, "single_attack", "{F2}")
dict_add($dict_spoil_keys, "sweep", "{F7}")
dict_add($dict_spoil_keys, "attack", "{F5}")
dict_add($dict_spoil_keys, "assist", "{F3}")
dict_add($dict_spoil_keys, "spoil", "{F6}")
dict_add($dict_spoil_keys, "follow", "{F4}")
dict_add($dict_spoil_keys, "next_target", "{F3}")

Local $dict_bountyHunter = dict()
dict_add($dict_bountyHunter, "MP", 0)
dict_add($dict_bountyHunter, "Follow_HP", 0)
Local $process_pool[12]
Local $hotkey_pool[12]
Local $skill_pool[12]
Local $status_pid[6]
#include "UI/userInterface.au3"


searchPid()

While 1
    $listener = GUIGetMsg()

    Switch $listener
        Case $GUI_EVENT_CLOSE
            Exit

    EndSwitch
    Sleep(1)
 WEnd




