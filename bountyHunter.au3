#RequireAdmin
#include <udf.au3>
#include <python_udf.au3>
#include <dictionary.au3>

Local $dict = dict()
dict_add($dict, "sweep", "{F7}")
dict_add($dict, "attack", "{F5}")
dict_add($dict, "spoil", "{F6}")
dict_add($dict, "follow", "{F4}")
dict_add($dict, "next_target", "{F3}")


Local $bh_pid = $CmdLine[1]
Local $current_hp = $CmdLine[2]
Local $percent = $CmdLine[3]
Local $command = $CmdLine[4]

If $bh_pid <> "" Then
   activateBountyHunter($dict, $bh_pid, $current_hp, $percent, $command)
EndIf
$dict = null
Exit

Func activateBountyHunter($dict, $pid, $hp, $percent, $command)
   Local $hotkey = $dict.item($command)
   Local $process = getProcess($pid)
   ControlSend($process, "", "", $hotkey)
EndFunc