#include-once
; 0 - lightest, 4 - darkest
Local $BLUE[] = ['0x4F9FE1', '0x248BE0', '0x097EE0', '0x045CA5', '0x03477F']

Func modify_buttons($object)
    GUICtrlSetFont($object, 8, 800)
	GUICtrlSetColor($object, '0xffffff')
	GUICtrlSetBkColor(-1, $BLUE[2])
 EndFunc

 Func modify_combos($object)
	GUICtrlSetFont($object, 8, 800)
 EndFunc

; modify color and text
Func modify_gui($gui_object, $color = 'default', $text = 'default')
   If not ($color == 'default') Then
	  GUICtrlSetBkColor($gui_object, $color)
   EndIf
   If not ($text == 'default') Then
	  GUICtrlSetData($gui_object, $text)
   EndIf
EndFunc

Func getProcess($id)
    Global $number
    if $id <> 0 Then
        $wl = WinList()
        Do
            For $i = 1 To $wl[0][0] Step 1
                If $wl[$i][0] <> "" Then
                    $PID = WinGetProcess($wl[$i][1])
                    If $PID == $id Then
                        $number = $wl[$i][1] ;HWND
                        ExitLoop
                    EndIf
                EndIf
			 Next
	    Until $number <> ""
        Return $number
    EndIf
EndFunc


Func getIndex($element, $array)
   Local $i = 0
   For $arg in $array
	  If $element == GUICtrlRead($arg) Then
		 return $i
	  Else
		 $i = $i + 1
	  EndIf
   Next
   Return -1
EndFunc

Func charAt($element, $array)
   Local $i = 0
   For $arg in $array
	  If $element == $arg Then
		 return $i
	  Else
		 $i = $i + 1
	  EndIf
   Next
   Return -1
EndFunc

Func read_file($file)
   Local $open_file = FileOpen($file, 0)
   Local $content = FileRead($open_file)
   FileClose($open_file)
   return StringSplit($content, @CRLF, 3)
EndFunc

Func inArray($element, $array)
   For $arg in $array
	  If $arg == $element Then
		 Return True
	  EndIf
   Next
   Return False
EndFunc

Func createProgressBar($x, $y, $width, $height, $color)
   $XS_n = DllCall("uxtheme.dll", "int", "GetThemeAppProperties")
   DllCall("uxtheme.dll", "none", "SetThemeAppProperties", "int", 0)

   local $bar = GUICtrlCreateProgress($x, $y, $width, $height, $PBS_SMOOTH)
   GUICtrlSetColor($bar, $color)
   DllCall("uxtheme.dll", "none", "SetThemeAppProperties", "int", $XS_n[0])

   return $bar
EndFunc

Func XPStyleToggle($OnOff = 1)
    If Not StringInStr(@OSTYPE, "WIN32_NT") Then Return 0
    If $OnOff Then
        $XS_n = DllCall("uxtheme.dll", "int", "GetThemeAppProperties")
        DllCall("uxtheme.dll", "none", "SetThemeAppProperties", "int", 0)
        Return 1
    ElseIf IsArray($XS_n) Then
        DllCall("uxtheme.dll", "none", "SetThemeAppProperties", "int", $XS_n[0])
        $XS_n = ""
        Return 1
    EndIf
    Return 0
EndFunc